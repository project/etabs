<?php

namespace Drupal\etabs;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Url;

/**
 * @internal
 */
final class TabData {

  protected RouteProviderInterface $routeProvider;

  protected EntityTypeManagerInterface $entityTypeManager;

  protected Url $url;

  /**
   * @var string|\Stringable
   */
  protected $title;

  protected bool $active;

  protected AccessResultInterface $accessResult;

  public function __construct(RouteProviderInterface $routeProvider, EntityTypeManagerInterface $entityTypeManager, Url $url, $title, bool $active, AccessResultInterface $accessResult) {
    $this->routeProvider = $routeProvider;
    $this->entityTypeManager = $entityTypeManager;
    $this->url = $url;
    $this->title = $title;
    $this->active = $active;
    $this->accessResult = $accessResult;
  }


  /**
   * @see \Drupal\Core\Menu\LocalTaskManager::getTasksBuild
   */
  public static function fromTabRenderArray(array $tabRenderArray): self {
    return new self(
      \Drupal::service('router.route_provider'),
      \Drupal::service('entity_type.manager'),
      $tabRenderArray['#link']['url'],
      $tabRenderArray['#link']['title'],
      $tabRenderArray['#active'],
      $tabRenderArray['#access'],
    );
  }

  public function isActive(): bool {
    return $this->active;
  }

  public function getUrl(): Url {
    return $this->url;
  }

  public function getAccessResult(): AccessResultInterface {
    return $this->accessResult;
  }

  public function getFormMode(): ?string {
    // @todo fix this, it's not really form mode.
    if ($this->url->isRouted()) {
      $route = $this->routeProvider->getRouteByName($this->url->getRouteName());
      if ($entityFormSpec = $route->getDefault('_entity_form')) {
        [$entityType, $formMode] = explode('.', $entityFormSpec);
        return $formMode;
      }
    }
    return NULL;
  }

  /**
   * Convert to link
   *
   * Beware: Re-add cacheability, it's lost here!
   *
   * @see \Drupal\Core\Render\Element\Dropbutton
   * @see \template_preprocess_links
   */
  public function toLink(): array {
    return [
      'title' => $this->title,
      'url' => $this->url,
      'attributes' => $this->active ? ['class' => ['is-active']] : [],
    ];
  }

}
