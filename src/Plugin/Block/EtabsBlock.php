<?php

namespace Drupal\etabs\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\LocalTaskManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Url;
use Drupal\etabs\TabData;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the "Etabs" block.
 *
 * @internal
 *
 * @Block(
 *   id = "etabs",
 *   admin_label = @Translation("Primary ETabs"),
 *   category = @Translation("ETabs"),
 * )
 */
class EtabsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  protected LocalTaskManagerInterface $localTaskManager;

  protected RouteMatchInterface $routeMatch;

  protected RouteProviderInterface $routeProvider;

  protected EntityTypeManagerInterface $entityTypeManager;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
    $instance->localTaskManager = $container->get('plugin.manager.menu.local_task');
    $instance->routeMatch = $container->get('current_route_match');
    $instance->routeProvider = $container->get('router.route_provider');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  public function build() {
    $regularTabs = $extraTabs = [];
    $primaryTabs = $this->getPrimaryTabs();
    $cacheability = CacheableMetadata::createFromRenderArray($primaryTabs);
    foreach (Element::children($primaryTabs) as $tabKey) {
      $tabRenderArray = $primaryTabs[$tabKey];
      $tab = TabData::fromTabRenderArray($tabRenderArray);
      if ($this->isExtraTab($tab->getUrl())) {
        $extraTabs[$tabKey] = $tabRenderArray;
      }
      else {
        $regularTabs[$tabKey] = $tabRenderArray;
      }
    }
    $this->adjustRegularTabs($regularTabs);
    $this->adjustExtraTabs($extraTabs);

    $build = [
      '#theme' => 'etabs',
      '#regular' => $this->buildRegularTabs($regularTabs),
      '#extra' => $this->buildExtraTabs($extraTabs)
    ];
    $cacheability->applyTo($build);
    return $build;
  }

  /**
   * Check if extra tab
   *
   * Extra tabs are
   * - Any entity form (like edit or delete).
   * - Any revisions tab.
   * - Any clone tab
   */
  public function isExtraTab(Url $url): bool {
    if ($url->isRouted()) {
      $routeName = $url->getRouteName();
      $route = $this->routeProvider->getRouteByName($routeName);
      if ($entityFormSpec = $route->getDefault('_entity_form')) {
        // Any entity form (like edit or delete).
        [$entityTypeId] = explode('.', $entityFormSpec);
        $entityType = $this->entityTypeManager->getDefinition($entityTypeId);
        assert($entityType instanceof EntityTypeInterface);
        /** @noinspection PhpUnnecessaryLocalVariableInspection */
        $isContentEntity = $entityType instanceof ContentEntityTypeInterface;
        // Edit OR delete tab of a content entity.
        return $isContentEntity;
      }
      elseif (preg_match('{entity.[^.]+.version_history}', $routeName)) {
        // Any revisions tab.
        return TRUE;
      }
      elseif ($routeName === 'quick_node_clone.node.quick_clone') {
        // Any clone tab from quick_node_clone.
        return TRUE;
      }
      // @todo Add a whitelist of more extra tabs here.
    }
    return FALSE;
  }

  /**
   * Adjust regular tabs
   *
   * What we want:
   * - A single regular "view" tab is not displayed (it would be confusing, and
   *   also that's what core does for primary tabs too).
   * - ...except if current page is another tab: Then we need it to switch back.
   */
  protected function adjustRegularTabs(array &$regularTabs): void {
    // Hide single regular tab, if it's active.
    // If it's not active, we need it to switch back.
    $visibleRegularTabs = array_filter(
      $regularTabs,
      fn(array $tab) => TabData::fromTabRenderArray($tab)->getAccessResult()->isAllowed()
    );
    if (count($visibleRegularTabs) === 1) {
      $singleRegularTab = TabData::fromTabRenderArray(reset($visibleRegularTabs));
      if ($singleRegularTab->isActive()) {
        $regularTabs = [];
      }
    }
  }

  /**
   * Adjust extra tabs
   *
   * What we want:
   * - On the default entity view, all extra tabs are displayed, in same order
   *   (so "edit" or "layout" is on top, whichever exists and comes first):
   * - On a FOO entity view, extra tab is single "Edit foo" (if exists).
   * - On any other regular tab, no extra tabs are displayed.
   * - On any extra tab, only that tab is displayed.
   */
  protected function adjustExtraTabs(array &$extraTabs): void {
    $activeExtraTabKey = NULL;
    foreach (Element::children($extraTabs) as $tabKey) {
      $tab = TabData::fromTabRenderArray($extraTabs[$tabKey]);
      if ($tab->isActive()) {
        $activeExtraTabKey = $tabKey;
        break;
      }
    }
    // We can't use the view mode of the active tab here, as there may be
    // secondary tabs.
    $currentEntityViewMode = $this->getCurrentEntityViewMode();

    /** @noinspection PhpStatementHasEmptyBodyInspection */
    if ($currentEntityViewMode === 'default') {
      // On the default entity view, all extra tabs are displayed, in same order
    }
    elseif ($currentEntityViewMode) {
      // On "latest", only show default edit tab.
      $matchingFormMode = $currentEntityViewMode === 'default.latest' ?
        'edit' : $currentEntityViewMode;
      // On a FOO entity view, extra tab is single "Edit foo" (if exists and has
      // access).
      $editTabKey = NULL;
      foreach (Element::children($extraTabs) as $tabKey) {
        $tab = TabData::fromTabRenderArray($extraTabs[$tabKey]);
        if (
          ($tabEntityFormMode = $tab->getFormMode())
          && ($tabEntityFormMode === $matchingFormMode)
          && $tab->getAccessResult()->isAllowed()
        ) {
          $editTabKey = $tabKey;
        }
      }
      if ($editTabKey) {
        $extraTabs = [$editTabKey => $extraTabs[$editTabKey]];
      }
      else {
        $extraTabs = [];
      }
    }
    elseif (!$activeExtraTabKey) {
      // On any other regular tab, no extra tabs are displayed.
      $extraTabs = [];
    }
    else {
      // On any extra tab, only that tab is displayed.
      $extraTabs = [$activeExtraTabKey => $extraTabs[$activeExtraTabKey]];
    }
  }

  protected function buildRegularTabs(array $regularTabs): array {
    return $regularTabs ? [
      '#theme' => 'menu_local_tasks',
      '#primary' => $regularTabs,
    ] : [];
  }

  private function buildExtraTabs(array $extraTabs): array {
    return $extraTabs ? $this->buildDropButtonFromTabs($extraTabs) : [];
  }

  public function defaultConfiguration() {
    return [
      'label_display' => FALSE,
    ];
  }

  /**
   * @see \Drupal\Core\Menu\Plugin\Block\LocalTasksBlock::build
   */
  protected function getPrimaryTabs(): array {
    $cacheability = new CacheableMetadata();
    $cacheability->addCacheableDependency($this->localTaskManager);
    $links = $this->localTaskManager->getLocalTasks($this->routeMatch->getRouteName(), 0);
    $cacheability = $cacheability->merge($links['cacheability']);
    $tabsCount = count(Element::getVisibleChildren($links['tabs']));
    $build = $tabsCount > 1 ? $links['tabs'] : [];
    $cacheability->applyTo($build);
    return $build;
  }

  /**
   * Get current entity view mode.
   *
   * Unfortunately the _entity_view default is unset here.
   * @see \Drupal\Core\Entity\Enhancer\EntityRouteEnhancer::enhanceEntityView
   */
  private function getCurrentEntityViewMode(): ?string {
    $routeName = $this->routeMatch->getRouteName();
    $routeObject = $this->routeMatch->getRouteObject();

    if (preg_match('{entity.[^.]+.canonical}', $routeName)) {
      // Really, it's something like "full".
      return 'default';
    }

    if (preg_match('{entity.[^.]+.latest_version}', $routeName)) {
      return 'default.latest';
    }

    if (
      $routeObject->getDefault('_entity')
      && ($viewMode = $routeObject->getDefault('view_mode'))
    ) {
      return $viewMode;
    }

    return NULL;
  }

  /**
   * @see \Drupal\Core\Render\Element\Dropbutton
   * @see \Drupal\Core\Menu\LocalTaskManager::getTasksBuild
   * @see \template_preprocess_links
   */
  private function buildDropButtonFromTabs(array $tabs): array {
    $links = [];
    $cacheability = CacheableMetadata::createFromRenderArray($tabs);
    foreach (Element::children($tabs) as $key) {
      $tabAsRenderArray = $tabs[$key];
      $tab = TabData::fromTabRenderArray($tabAsRenderArray);
      if ($tab->getAccessResult()->isAllowed()) {
        $links[$key] = $tab->toLink();
      }
      $cacheability->addCacheableDependency($tab->getAccessResult());
    }
    $build = [
      '#type' => 'dropbutton',
      '#dropbutton_type' => 'etabs',
      '#links' => $links,
    ];
    $cacheability->applyTo($build);
    return $build;
  }

}
